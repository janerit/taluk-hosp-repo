-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2021 at 03:55 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Talukhos_vaccination`
--

CREATE USER IF NOT EXISTS 'developer'@'localhost' IDENTIFIED BY 'passwd#1234';

CREATE DATABASE IF NOT EXISTS `Talukhos_vaccination` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

GRANT ALL PRIVILEGES ON Talukhos_vaccination . * TO 'developer'@'localhost';


USE `Talukhos_vaccination`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_balance`
--

CREATE TABLE `tbl_balance` (
  `vd_id` int(11) NOT NULL,
  `first_dose_remaining` int(11) NOT NULL,
  `second_dose_remaining` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_count_percentage`
--

CREATE TABLE `tbl_count_percentage` (
  `percentage_id` int(11) NOT NULL,
  `firstdose_percent` int(11) NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `usertype` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`username`, `password`, `usertype`) VALUES
('admin', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_panchayath`
--

CREATE TABLE `tbl_panchayath` (
  `panchayath_id` int(11) NOT NULL,
  `panchayath_name` varchar(50) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_public`
--

CREATE TABLE `tbl_public` (
  `public_id` bigint(20) NOT NULL,
  `aadhar` varchar(12) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `age` int(3) NOT NULL,
  `mobile_no` char(10) NOT NULL,
  `address` varchar(150) DEFAULT NULL,
  `panchayath_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_slot`
--

CREATE TABLE `tbl_slot` (
  `token_no` bigint(20) NOT NULL,
  `public_id` bigint(20) NOT NULL,
  `vd_id` int(11) NOT NULL,
  `dose_no` int(11) NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_vaccination_day`
--

CREATE TABLE `tbl_vaccination_day` (
  `vd_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `count` int(10) NOT NULL,
  `vaccine_id` int(11) NOT NULL,
  `starting_time` datetime NOT NULL,
  `ending_time` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_vaccine`
--

CREATE TABLE `tbl_vaccine` (
  `vaccine_id` int(11) NOT NULL,
  `vaccine_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_balance`
--
ALTER TABLE `tbl_balance`
  ADD PRIMARY KEY (`vd_id`);

--
-- Indexes for table `tbl_count_percentage`
--
ALTER TABLE `tbl_count_percentage`
  ADD PRIMARY KEY (`percentage_id`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `tbl_panchayath`
--
ALTER TABLE `tbl_panchayath`
  ADD PRIMARY KEY (`panchayath_id`);

--
-- Indexes for table `tbl_public`
--
ALTER TABLE `tbl_public`
  ADD PRIMARY KEY (`public_id`),
  ADD KEY `panchayath_id` (`panchayath_id`);

--
-- Indexes for table `tbl_slot`
--
ALTER TABLE `tbl_slot`
  ADD PRIMARY KEY (`token_no`,`vd_id`),
  ADD KEY `public_id` (`public_id`),
  ADD KEY `vd_id` (`vd_id`);

--
-- Indexes for table `tbl_vaccination_day`
--
ALTER TABLE `tbl_vaccination_day`
  ADD PRIMARY KEY (`vd_id`),
  ADD KEY `tbl_vaccination_day_ibfk_1` (`vaccine_id`);

--
-- Indexes for table `tbl_vaccine`
--
ALTER TABLE `tbl_vaccine`
  ADD PRIMARY KEY (`vaccine_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_count_percentage`
--
ALTER TABLE `tbl_count_percentage`
  MODIFY `percentage_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_panchayath`
--
ALTER TABLE `tbl_panchayath`
  MODIFY `panchayath_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_public`
--
ALTER TABLE `tbl_public`
  MODIFY `public_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_vaccination_day`
--
ALTER TABLE `tbl_vaccination_day`
  MODIFY `vd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_vaccine`
--
ALTER TABLE `tbl_vaccine`
  MODIFY `vaccine_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_balance`
--
ALTER TABLE `tbl_balance`
  ADD CONSTRAINT `tbl_balance_ibfk_1` FOREIGN KEY (`vd_id`) REFERENCES `tbl_vaccination_day` (`vd_id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_public`
--
ALTER TABLE `tbl_public`
  ADD CONSTRAINT `tbl_public_ibfk_1` FOREIGN KEY (`panchayath_id`) REFERENCES `tbl_panchayath` (`panchayath_id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_slot`
--
ALTER TABLE `tbl_slot`
  ADD CONSTRAINT `tbl_slot_ibfk_1` FOREIGN KEY (`public_id`) REFERENCES `tbl_public` (`public_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_slot_ibfk_2` FOREIGN KEY (`vd_id`) REFERENCES `tbl_vaccination_day` (`vd_id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_vaccination_day`
--
ALTER TABLE `tbl_vaccination_day`
  ADD CONSTRAINT `tbl_vaccination_day_ibfk_1` FOREIGN KEY (`vaccine_id`) REFERENCES `tbl_vaccine` (`vaccine_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
