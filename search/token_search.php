<?php
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//Including database connection file
include "../connection.php" ;
?>
<!DOCTYPE html>
<html>
<head>
  <title>Search by Token</title>
  <!-- <link rel="stylesheet" href="../ktfo_css.css"> -->
		<link rel="stylesheet" type="text/css" href="../css.css">
<style>
.invalid { color:#FF0000;  font-size:22px; font-style:italic; text-align:center;}
</style>
</head>
<body>
<div>
<?php
include "../menu_for_folder.php";
?>
</div>
<!--textes for including token and date-->
<body>
<div class="form" width="1500px">
<form method="post">
  <table align="center">
    <tr>
      <th align="center" colspan="7">SEARCH BY TOKEN and DATE</th>
	</tr>



	<tr>
	  <td><label for="date1">Date</label></td><td><input type="date" name="date" id="date" style="width: 100%" value="<?php echo date('Y-m-d'); ?>"required></td>
       <td><label for="tkn1">Token Number</label></td><td><input name="tkn_num" placeholder="Enter token"style="width:100%" type="text" required></td>
			 <td> <label for="lacno">Choose Vaccine</label> </td>
			 <td><select name="vaccine_id" width="100%" required>
			 	<option value="" disabled selected>-Select-</option>
			 <?php
			 $records = mysqli_query($conn,"select * from tbl_vaccine");
	 while($data = mysqli_fetch_array($records))
	 {
				?>

	 <option value="<?php echo $data['vaccine_id']; ?> "><?php echo $data['vaccine_name']; ?></option>
	 <?php
 }
		?>
 </select></td> <td></td> <td></td>
		<td><button name="search" value="search" class="btn" >Search</button></td>
	</tr>
  </table>

	<?php

	   if(isset($_POST['search']))
	   {

		$date=$_POST['date'];
		$vaccine_id=$_POST['vaccine_id'];
	  $tkn_no=$_POST['tkn_num'];
		$query1="select  sl.token_no as TOKEN_NUMBER,
	    vcd.date as DATE,
		pb.aadhar as AADHAR_NUM,pb.name as NAME,pb.age as AGE,pb.mobile_no as MOBILE_NUMBER,pb.address as ADDRESS,
		vc.vaccine_name as VACCINE_NAME,
		pc.panchayath_name AS PANCHAYATH_NAME
		FROM tbl_vaccination_day vcd
	    inner join tbl_slot sl on sl.vd_id=vcd.vd_id
		inner join tbl_public pb on pb.public_id=sl.public_id
		inner join tbl_vaccine vc on vc.vaccine_id=vcd.vaccine_id
		inner join tbl_panchayath pc on pc.panchayath_id=pb.panchayath_id
	    where sl.token_no=$tkn_no and vcd.date='$date' and vcd.vaccine_id=$vaccine_id";


		$result=mysqli_query($conn,$query1);
			while($row=mysqli_fetch_array($result))
			{

				$tokn_num=$row['TOKEN_NUMBER'];
				$date=$row['DATE'];
				$aadar=$row['AADHAR_NUM'];
				$name=$row['NAME'];
				$age= $row['AGE'];
				$mobile_no=$row['MOBILE_NUMBER'];
				$address=$row['ADDRESS'];
				$vaccine_name=$row['VACCINE_NAME'];
				$panchayath_name=$row['PANCHAYATH_NAME'];
			}
	 ?>
			<?php if(mysqli_num_rows($result)>0)
			{ ?>
			<form action="" method="POST">
			<table align="center" border="1px" style="width:100%; line-Height:40px;">
		    <tr>
			<th colspan="9"><h2> PERSONAL DETAILS </h2></th>
		    </tr>
		    <tr>
				<td><label for="tkn">Token number</label></td><td><input type="text" name="Token_number" value="<?php echo $tokn_num?>"/></td></tr>
				<tr><td><label for="DATE">Date</label></td><td><input type="text" name="DATE" value="<?php echo $date?>"/></td></tr>
				<tr><td><label for="DATE">Aadhar Number</label></td><td><input type="text" name="AADHAR_NUM" value="<?php echo $aadar?>"/></td></tr>
				<tr><td><label for="DATE">Name</label></td><td><input type="text" name="NAME" value="<?php echo $name?>"/></td></tr>
				<tr><td><label for="DATE">Age</label></td><td><input type="text" name="AGE" value="<?php echo $age?>"/></td></tr>
				<tr><td><label for="DATE">Mobile Number</label></td><td><input type="text" name="MOBILE_NUMBER" value="<?php echo $mobile_no?>"/></td></tr>
				<tr><td><label for="DATE">Address</label></td><td><input type="text" name="ADDRESS" value="<?php echo $address?>"/></td></tr>
				<tr><td><label for="DATE">Vaccine Name</label></td><td><input type="text" name="VACCINE_NAME" value="<?php echo $vaccine_name?>"/></td></tr>
				<tr><td><label for="DATE">Panchayath Name</label></td><td><input type="text" name="PANCHAYATH_NAME" value="<?php echo $panchayath_name?>"/></td></tr>
			</table>
			</form>
			<?php

			}
			else{
				echo '<script>alert("No Data Found")</script>';
				echo '<script type="text/javascript">
					location.replace("token_search.php");
					</script>';
			}?>
		<?php

		}?>
</form>
</div>
<br><br><br><br><br><br>
<div style="position:relative; bottom:0; width:100%;">
<?php
//including footer file
include "../Footer.php";
?>
</div>
</table>
</form>
</body>
</html>
