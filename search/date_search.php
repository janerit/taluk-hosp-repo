<?php
 session_start();
 if(!isset($_SESSION['login_user']))
 {
 	echo "<script>alert('Session Expired');</script>";
 	echo '<script type="text/javascript">
 			location.replace("../index.php");
			</script>';
 }
//Including database connection file
include "../connection.php" ;
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Search By Date</title>
    <style media="screen">
  .datechooser
  {
    border:3px solid black;
    border-collapse:collapse;
    margin: auto;
     width: 720px;
    padding: 10px;
  }
  .print_button{
     width: 720px;
     padding: 10px;
  }
    </style>
    <link rel="stylesheet" type="text/css" href="../css.css">
	
	<?php
	include "../menu_for_folder.php" ;

	?>
  </head>
  <body>

    <form class="" action="#" method="post" name="reg_view">
      <div class="datechooser">
          <table>
              <tr>
              <th align="center" colspan="5">SEARCH BY DATE</th>
	          </tr>
		      <tr>
              <td> <label for="lacno">Choose Date</label> </td> <td></td>
              <td> <input type="date" name="datename" value="" required> </td> <td></td>
              <td> <label for="lacno">Choose Vaccine</label> </td> <td></td>
               	<td><select name="vaccine_id" required>
                  <option value="" disabled selected>-Select-</option>
          			<?php
          			$records = mysqli_query($conn,"select * from tbl_vaccine");
          	while($data = mysqli_fetch_array($records))
          	{
          			 ?>

          	<option value="<?php echo $data['vaccine_id']; ?> "><?php echo $data['vaccine_name']; ?></option>
          	<?php
          }
          	 ?>
          </select></td> <td></td> <td></td>
              <td> <input type="submit" name="submit" value="Search"> </td>

            </tr>
  				</table>
  			</div>
      </form>
      <?php

      if(isset($_POST['submit']))
      {
        $date=$_POST['datename'];
        $_SESSION['date']=$date;
        $vaccine_id=$_POST['vaccine_id'];
        $records = mysqli_query($conn,"SELECT * FROM tbl_slot s
          INNER JOIN tbl_public p on p.public_id=s.public_id
          INNER JOIN tbl_vaccination_day vd on s.vd_id=vd.vd_id
          INNER JOIN tbl_vaccine v on v.vaccine_id=vd.vaccine_id where vd.date='$date' and vd.vaccine_id=$vaccine_id");
          if($records->num_rows>0)
          {


            ?>
        <br><br>
        <div class="form">
  			<table  class="view_table">
    				<tr>
  					<th>Token No</th>
  					<th>Name</th>
            <th>Age</th>
            <th>Aadhar Number</th>
  					<th>Phone Number</th>
  					<th>Vaccine</th>
  					<th>Dose</th>
  					<!--<th colspan="2"></th>-->
    				</tr>

            <?php




              	while($data = mysqli_fetch_assoc($records))
              	{
              ?>
              				<tr>
                  				<td><?php echo $data['token_no']; ?></td>
              						<td><?php echo $data['name']; ?></td>
                          <td><?php echo $data['age']; ?></td>
                          <td><?php echo $data['aadhar']; ?></td>
                					<td><?php echo $data['mobile_no']; ?></td>
                					<td><?php echo $data['vaccine_name']; ?></td>
                          <td><?php echo $data['dose_no']; ?></td>
              				</tr>


              <?php
              }
              ?>
                </table>


              <?php
              }
              else {
                echo "<script>alert('No Data found for the Date');</script>";

        		echo '<script type="text/javascript">
        			location.replace("date_search.php");
        			</script>';
              }
            }

              ?>
              <br><br><br><br><br><br>
              <div style="position:relative; bottom:0; width:100%;">
              <?php
              //including footer file
              include "../Footer.php";
              ?>
              </div>
  </body>
</html>
