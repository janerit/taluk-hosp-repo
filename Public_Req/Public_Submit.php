<title>Registration Status</title>
<script type="text/javascript">
	function disableBack() { window.history.forward(); }
        setTimeout("disableBack()", 0);
        window.onunload = function () { null };
</script>

<style type="text/css">
	table
	{
		margin: auto;
		border-color: white;
		border-collapse: collapse;
		width: 80%;
	}
	tr
	{
		height: 30px;
	}
	tr:nth-child(odd)
	{
		background-color:silver;
		color: black;
	}
	th
	{
		background-color: #006699;
		color: white;
	}
	h3
	{
		color:white;
		background-color:red;
	}
	div
	{
		border: 4px double white;
		text-align: center;
		border-radius: 5px;
		margin: auto;
		width: 70%;
		padding: 20px;
	}
	a
	{
		color: white;
	}
	@media only screen and (max-width: 1000px)
	{
		div{margin-top: 50%;}
	}


</style>

<?php
include "../connection.php";
require_once('../TCPDF/examples/tcpdf_include.php');
require_once('../TCPDF/tcpdf.php');

session_start();
if(!isset($_SESSION['user_type']))
{
 	echo "<script>alert('Session Expired');</script>";
 	echo '<script type="text/javascript">
 			location.replace("index.php");
 			</script>';
}

if (isset($_REQUEST['submit']))
{
	$closing= "update tbl_vaccination_day set status =0 where starting_time < now() and status =1;";
	$close_res = $conn->query($closing);

	$content='<!DOCTYPE html><html><head><title></title></head><body><h1 align="center">TALUK HEAD QUARTERS HOSPITAL PAMPADY</h1><h3 align="center">Kottayam 686502</h3><h3 align="center">Email id: thqhpampady@gmail.com</h3><h3 align="center">Phone no.: 0481 2507866, 0481 2509966</h3><br /><br /><br /><br /><h2>Covid-19 Vaccination Registration Details</h2><br><br><table border="1" cellpadding="8" style="border-collapse: collapse"><tr align="center"><th><b>Token No.</b></th><th><b>Name</b></th><th><b>Age</b></th><th><b>Phone No.</b></th><th><b>Dose 1/2</b></th><th><b>Reporting Time</b></th></tr>';

	$flag=0;

	$vaccine_id=$_POST['vaccine1'];
    $name=$_POST['name1'];
	$age=intval($_POST['age1']);
	$aadhaar=$_POST['aadhaar_no1'];
	$address=$_POST['address1'];
	$panchayat=intval($_POST['panchayat1']);
	$phn_no=$_POST['phn_no1'];

	$res=$conn->query("SELECT v.count,v.date,v.vd_id FROM tbl_vaccination_day v WHERE v.status=1 and v.vaccine_id=$vaccine_id and v.date=(SELECT v.date from tbl_vaccination_day v where v.status=1 and v.vaccine_id=$vaccine_id ORDER BY v.date ASC LIMIT 1)");
	if ($res->num_rows)
	{
		$count=0;
		if ($row=$res->fetch_assoc())
		{
			$count=$row['count'];
			$date=$row['date'];
			$vd_id=intval($row['vd_id']);
		}
		$result=$conn->query("SELECT p.public_id FROM tbl_public p INNER JOIN tbl_slot s USING(public_id) WHERE p.name='$name' AND p.age=$age AND p.mobile_no='$phn_no' AND s.vd_id IN (SELECT v.vd_id FROM tbl_vaccination_day v WHERE v.date='$date')");
		if ($result->num_rows==0)
		{
			if($_POST['dose1']=='dose1')
				$f=1;
			else
				$f=2;
			if ($f==1)
			{
				$conn->query('BEGIN;');
				$res1=$conn->query("SELECT first_dose_remaining+second_dose_remaining as Balance FROM tbl_balance WHERE vd_id=$vd_id AND first_dose_remaining>0 for UPDATE");
				if ($row=$res1->fetch_assoc())
				{
					$balance=$row['Balance'];
					$res2=$conn->query("UPDATE tbl_balance SET first_dose_remaining=first_dose_remaining-1 WHERE vd_id=$vd_id");
					if($res2)
					{
						$token_no=$count-$balance+1;
						if ($token_no==1)
						{
							$res3=$conn->query("SELECT starting_time FROM tbl_vaccination_day WHERE vd_id=$vd_id");
							if($row3=$res3->fetch_assoc())
								$time=date('H:i:s',strtotime($row3['starting_time']));
						}
						else
						{
							$res3=$conn->query("SELECT time FROM tbl_slot WHERE token_no=$token_no-1 AND vd_id=$vd_id");
							if ($row3=$res3->fetch_assoc())
							{
								$time=$row3['time'];
								if ($token_no%10==1)
									$time=date('H:i:s',strtotime('+15 minutes',strtotime($time)));
							}
						}

						$conn->query("UPDATE tbl_vaccination_day SET status=0 WHERE vd_id=(SELECT vd_id FROM tbl_balance WHERE vd_id=$vd_id AND first_dose_remaining=0 AND second_dose_remaining=0)");

						$query1=$conn->query("INSERT INTO tbl_public(aadhar,name,age,mobile_no,address,panchayath_id) VALUES ('$aadhaar','$name',$age,'$phn_no','$address',$panchayat)");
						$res4=$conn->query("SELECT public_id FROM tbl_public WHERE name='$name' AND age=$age AND mobile_no='$phn_no'");
						if($row4=$res4->fetch_assoc())
							$public_id=intval($row4['public_id']);
						$query2=$conn->query("INSERT INTO tbl_slot (token_no,public_id,vd_id,dose_no,time) VALUES ($token_no,$public_id,$vd_id,$f,'$time')");
						if ($query2)
						{
							$flag=1;
							$array[$name]="Your Covid Vaccine Registration is Successful";
							$date=date('d/m/Y',strtotime($date));
							$res5=$conn->query("SELECT vaccine_name FROM tbl_vaccine WHERE vaccine_id=$vaccine_id");
							if ($row5=$res5->fetch_assoc())
							{
								$vaccine_name=$row5['vaccine_name'];
							}
							$content.='<tr><td>'.$token_no.' - '.$vaccine_name.'</td><td>'.$name.'</td><td>'.$age.'</td><td>'.$phn_no.'</td><td>'.$f.'</td><td>'.$date.' '.$time.'</td></tr>';
						}
					}
					$conn->query('COMMIT;');
				}
				else
				{
				   $array[$name]="Sorry, Available Slots Got Completely Booked. Please Try Another Day";
				   $conn->query('ROLLBACK;');
				}
			}
			elseif ($f==2)
			{
				$conn->query('BEGIN;');
				$res1=$conn->query("SELECT first_dose_remaining+second_dose_remaining as Balance FROM tbl_balance WHERE vd_id=$vd_id AND second_dose_remaining>0 for UPDATE");
				if ($row=$res1->fetch_assoc())
				{
					$balance=$row['Balance'];
					$res2=$conn->query("UPDATE tbl_balance SET second_dose_remaining=second_dose_remaining-1 WHERE vd_id=$vd_id");
					if($res2)
					{
						$token_no=$count-$balance+1;
						if ($token_no==1)
						{
							$res3=$conn->query("SELECT starting_time FROM tbl_vaccination_day WHERE vd_id=$vd_id");
							if($row3=$res3->fetch_assoc())
							{
								$time=date('H:i:s',strtotime($row3['starting_time']));
							}
						}
						else
						{
							$res3=$conn->query("SELECT time FROM tbl_slot WHERE token_no=$token_no-1 AND vd_id=$vd_id");
							if ($row3=$res3->fetch_assoc())
							{
								$time=$row3['time'];
								if ($token_no%10==1)
								{
									$time=date('H:i:s',strtotime('+15 minutes',strtotime($time)));
								}
							}
						}

						$conn->query("UPDATE tbl_vaccination_day SET status=0 WHERE vd_id=(SELECT vd_id FROM tbl_balance WHERE vd_id=$vd_id AND first_dose_remaining=0 AND second_dose_remaining=0)");

						$res4=$conn->query("SELECT public_id FROM tbl_public WHERE name='$name' AND age=$age AND mobile_no='$phn_no'");
						if($row4=$res4->fetch_assoc())
						{
							$public_id=intval($row4['public_id']);
						}
						else
						{
							$query1=$conn->query("INSERT INTO tbl_public(aadhar,name,age,mobile_no,address,panchayath_id) VALUES ('$aadhaar','$name',$age,'$phn_no','$address',$panchayat)");
							$res4=$conn->query("SELECT public_id FROM tbl_public WHERE name='$name' AND age=$age AND mobile_no='$phn_no'");
						   if($row4=$res4->fetch_assoc())
						   {
								$public_id=intval($row4['public_id']);
						   }
						}
						$query2=$conn->query("INSERT INTO tbl_slot (token_no,public_id,vd_id,dose_no,time) VALUES ($token_no,$public_id,$vd_id,$f,'$time')");
						if ($query2)
						{
							$flag=1;
							$array[$name]="Your Covid Vaccine Registration is Successful";
							$date=date('d/m/Y',strtotime($date));
							$res5=$conn->query("SELECT vaccine_name FROM tbl_vaccine WHERE vaccine_id=$vaccine_id");
							if ($row5=$res5->fetch_assoc())
							{
								$vaccine_name=$row5['vaccine_name'];
							}
							$content.='<tr><td>'.$token_no.' - '.$vaccine_name.'</td><td>'.$name.'</td><td>'.$age.'</td><td>'.$phn_no.'</td><td>'.$f.'</td><td>'.$date.' '.$time.'</td></tr>';
						}
					}
					$conn->query('COMMIT;');
				}
				else
				{
					$array[$name]="Sorry, Available Slots Got Completely Booked. Please Try Another Day";
					$conn->query('ROLLBACK;');
				}
			}
		}
		else
		{
			$array[$name]="Already Registered";
		}
	}
	else
	{
	   $array[$name]="Sorry, Available Slots Got Completely Booked. Please Try Another Day";
	}

	if(isset($_POST['check2'])&&$_POST['check2']=='check')
	{
		$vaccine_id=$_POST['vaccine2'];
		$name=$_POST['name2'];
		$age=intval($_POST['age2']);
		$aadhaar=$_POST['aadhaar_no2'];
		$address=$_POST['address2'];
		$panchayat=intval($_POST['panchayat2']);
		$phn_no=$_POST['phn_no2'];

		$res=$conn->query("SELECT v.count,v.date,v.vd_id FROM tbl_vaccination_day v WHERE v.status=1 and v.vaccine_id=$vaccine_id and v.date=(SELECT v.date from tbl_vaccination_day v where v.status=1 and v.vaccine_id=$vaccine_id ORDER BY v.date ASC LIMIT 1)");
		if ($res->num_rows)
		{
			$count=0;
			if ($row=$res->fetch_assoc())
			{
				$count=$row['count'];
				$date=$row['date'];
				$vd_id=intval($row['vd_id']);
			}

			$result=$conn->query("SELECT p.public_id FROM tbl_public p INNER JOIN tbl_slot s USING(public_id) WHERE p.name='$name' AND p.age=$age AND p.mobile_no='$phn_no' AND s.vd_id IN (SELECT v.vd_id FROM tbl_vaccination_day v WHERE v.date='$date')");
			if ($result->num_rows==0)
			{
				if($_POST['dose2']=='dose1')
					$f=1;
				else
					$f=2;
				if ($f==1)
				{
					$conn->query('BEGIN;');
					$res1=$conn->query("SELECT first_dose_remaining+second_dose_remaining as Balance FROM tbl_balance WHERE vd_id=$vd_id AND first_dose_remaining>0 for UPDATE");
					if ($row=$res1->fetch_assoc())
					{
						$balance=$row['Balance'];
						$res2=$conn->query("UPDATE tbl_balance SET first_dose_remaining=first_dose_remaining-1 WHERE vd_id=$vd_id");
						if($res2)
						{
							$token_no=$count-$balance+1;
							if ($token_no==1)
							{
								$res3=$conn->query("SELECT starting_time FROM tbl_vaccination_day WHERE vd_id=$vd_id");
								if($row3=$res3->fetch_assoc())
									$time=date('H:i:s',strtotime($row3['starting_time']));
							}
							else
							{
								$res3=$conn->query("SELECT time FROM tbl_slot WHERE token_no=$token_no-1 AND vd_id=$vd_id");
								if ($row3=$res3->fetch_assoc())
								{
									$time=$row3['time'];
									if ($token_no%10==1)
										$time=date('H:i:s',strtotime('+15 minutes',strtotime($time)));
								}
							}

							$conn->query("UPDATE tbl_vaccination_day SET status=0 WHERE vd_id=(SELECT vd_id FROM tbl_balance WHERE vd_id=$vd_id AND first_dose_remaining=0 AND second_dose_remaining=0)");

							$query1=$conn->query("INSERT INTO tbl_public(aadhar,name,age,mobile_no,address,panchayath_id) VALUES ('$aadhaar','$name',$age,'$phn_no','$address',$panchayat)");
							$res4=$conn->query("SELECT public_id FROM tbl_public WHERE name='$name' AND age=$age AND mobile_no='$phn_no'");
							if($row4=$res4->fetch_assoc())
								$public_id=intval($row4['public_id']);
							$query2=$conn->query("INSERT INTO tbl_slot (token_no,public_id,vd_id,dose_no,time) VALUES ($token_no,$public_id,$vd_id,$f,'$time')");
							if ($query2)
							{
								$flag=1;
								$array[$name]="Your Covid Vaccine Registration is Successful";
								$date=date('d/m/Y',strtotime($date));
								$res5=$conn->query("SELECT vaccine_name FROM tbl_vaccine WHERE vaccine_id=$vaccine_id");
								if ($row5=$res5->fetch_assoc())
								{
									$vaccine_name=$row5['vaccine_name'];
								}
								$content.='<tr><td>'.$token_no.' - '.$vaccine_name.'</td><td>'.$name.'</td><td>'.$age.'</td><td>'.$phn_no.'</td><td>'.$f.'</td><td>'.$date.' '.$time.'</td></tr>';
							}
						}
						$conn->query('COMMIT;');
					}
					else
					{
					   $array[$name]="Sorry, Available Slots Got Completely Booked. Please Try Another Day";
					   $conn->query('ROLLBACK;');
					}
				}
				elseif ($f==2)
				{
					$conn->query('BEGIN;');
					$res1=$conn->query("SELECT first_dose_remaining+second_dose_remaining as Balance FROM tbl_balance WHERE vd_id=$vd_id AND second_dose_remaining>0 for UPDATE");
					if ($row=$res1->fetch_assoc())
					{
						$balance=$row['Balance'];
						$res2=$conn->query("UPDATE tbl_balance SET second_dose_remaining=second_dose_remaining-1 WHERE vd_id=$vd_id");
						if($res2)
						{
							$token_no=$count-$balance+1;
							if ($token_no==1)
							{
								$res3=$conn->query("SELECT starting_time FROM tbl_vaccination_day WHERE vd_id=$vd_id");
								if($row3=$res3->fetch_assoc())
								{
									$time=date('H:i:s',strtotime($row3['starting_time']));
								}
							}
							else
							{
								$res3=$conn->query("SELECT time FROM tbl_slot WHERE token_no=$token_no-1 AND vd_id=$vd_id");
								if ($row3=$res3->fetch_assoc())
								{
									$time=$row3['time'];
									if ($token_no%10==1)
									{
										$time=date('H:i:s',strtotime('+15 minutes',strtotime($time)));
									}
								}
							}

							$conn->query("UPDATE tbl_vaccination_day SET status=0 WHERE vd_id=(SELECT vd_id FROM tbl_balance WHERE vd_id=$vd_id AND first_dose_remaining=0 AND second_dose_remaining=0)");

							$res4=$conn->query("SELECT public_id FROM tbl_public WHERE name='$name' AND age=$age AND mobile_no='$phn_no'");
							if($row4=$res4->fetch_assoc())
							{
								$public_id=intval($row4['public_id']);
							}
							else
							{
								$query1=$conn->query("INSERT INTO tbl_public(aadhar,name,age,mobile_no,address,panchayath_id) VALUES ('$aadhaar','$name',$age,'$phn_no','$address',$panchayat)");
								$res4=$conn->query("SELECT public_id FROM tbl_public WHERE name='$name' AND age=$age AND mobile_no='$phn_no'");
							   if($row4=$res4->fetch_assoc())
							   {
									$public_id=intval($row4['public_id']);
							   }
							}
							$query2=$conn->query("INSERT INTO tbl_slot (token_no,public_id,vd_id,dose_no,time) VALUES ($token_no,$public_id,$vd_id,$f,'$time')");
							if ($query2)
							{
								$flag=1;
								$array[$name]="Your Covid Vaccine Registration is Successful";
								$date=date('d/m/Y',strtotime($date));
								$res5=$conn->query("SELECT vaccine_name FROM tbl_vaccine WHERE vaccine_id=$vaccine_id");
								if ($row5=$res5->fetch_assoc())
								{
									$vaccine_name=$row5['vaccine_name'];
								}
								$content.='<tr><td>'.$token_no.' - '.$vaccine_name.'</td><td>'.$name.'</td><td>'.$age.'</td><td>'.$phn_no.'</td><td>'.$f.'</td><td>'.$date.' '.$time.'</td></tr>';
							}
						}
						$conn->query('COMMIT;');
					}
					else
					{
						$array[$name]="Sorry, Available Slots Got Completely Booked. Please Try Another Day";
						$conn->query('ROLLBACK;');
					}
				}
			}
			else
			{
				$array[$name]="Already Registered";
			}
		}
		else
		{
		   $array[$name]="Sorry, Available Slots Got Completely Booked. Please Try Another Day";
		}
	}

	if(isset($_POST['check3'])&&$_POST['check3']=='check')
	{
		$vaccine_id=$_POST['vaccine3'];
		$name=$_POST['name3'];
		$age=intval($_POST['age3']);
		$aadhaar=$_POST['aadhaar_no3'];
		$address=$_POST['address3'];
		$panchayat=intval($_POST['panchayat3']);
		$phn_no=$_POST['phn_no3'];

		$res=$conn->query("SELECT v.count,v.date,v.vd_id FROM tbl_vaccination_day v WHERE v.status=1 and v.vaccine_id=$vaccine_id and v.date=(SELECT v.date from tbl_vaccination_day v where v.status=1 and v.vaccine_id=$vaccine_id ORDER BY v.date ASC LIMIT 1)");
		if ($res->num_rows)
		{
			$count=0;
			if ($row=$res->fetch_assoc())
			{
				$count=$row['count'];
				$date=$row['date'];
				$vd_id=intval($row['vd_id']);
			}

			$result=$conn->query("SELECT p.public_id FROM tbl_public p INNER JOIN tbl_slot s USING(public_id) WHERE p.name='$name' AND p.age=$age AND p.mobile_no='$phn_no' AND s.vd_id IN (SELECT v.vd_id FROM tbl_vaccination_day v WHERE v.date='$date')");
			if ($result->num_rows==0)
			{
				if($_POST['dose3']=='dose1')
					$f=1;
				else
					$f=2;
				if ($f==1)
				{
					$conn->query('BEGIN;');
					$res1=$conn->query("SELECT first_dose_remaining+second_dose_remaining as Balance FROM tbl_balance WHERE vd_id=$vd_id AND first_dose_remaining>0 for UPDATE");
					if ($row=$res1->fetch_assoc())
					{
						$balance=$row['Balance'];
						$res2=$conn->query("UPDATE tbl_balance SET first_dose_remaining=first_dose_remaining-1 WHERE vd_id=$vd_id");
						if($res2)
						{
							$token_no=$count-$balance+1;
							if ($token_no==1)
							{
								$res3=$conn->query("SELECT starting_time FROM tbl_vaccination_day WHERE vd_id=$vd_id");
								if($row3=$res3->fetch_assoc())
									$time=date('H:i:s',strtotime($row3['starting_time']));
							}
							else
							{
								$res3=$conn->query("SELECT time FROM tbl_slot WHERE token_no=$token_no-1 AND vd_id=$vd_id");
								if ($row3=$res3->fetch_assoc())
								{
									$time=$row3['time'];
									if ($token_no%10==1)
										$time=date('H:i:s',strtotime('+15 minutes',strtotime($time)));
								}
							}

							$conn->query("UPDATE tbl_vaccination_day SET status=0 WHERE vd_id=(SELECT vd_id FROM tbl_balance WHERE vd_id=$vd_id AND first_dose_remaining=0 AND second_dose_remaining=0)");

							$query1=$conn->query("INSERT INTO tbl_public(aadhar,name,age,mobile_no,address,panchayath_id) VALUES ('$aadhaar','$name',$age,'$phn_no','$address',$panchayat)");
							$res4=$conn->query("SELECT public_id FROM tbl_public WHERE name='$name' AND age=$age AND mobile_no='$phn_no'");
							if($row4=$res4->fetch_assoc())
								$public_id=intval($row4['public_id']);
							$query2=$conn->query("INSERT INTO tbl_slot (token_no,public_id,vd_id,dose_no,time) VALUES ($token_no,$public_id,$vd_id,$f,'$time')");
							if ($query2)
							{
								$flag=1;
								$array[$name]="Your Covid Vaccine Registration is Successful";
								$date=date('d/m/Y',strtotime($date));
								$res5=$conn->query("SELECT vaccine_name FROM tbl_vaccine WHERE vaccine_id=$vaccine_id");
								if ($row5=$res5->fetch_assoc())
								{
									$vaccine_name=$row5['vaccine_name'];
								}
								$content.='<tr><td>'.$token_no.' - '.$vaccine_name.'</td><td>'.$name.'</td><td>'.$age.'</td><td>'.$phn_no.'</td><td>'.$f.'</td><td>'.$date.' '.$time.'</td></tr>';
							}
						}
						$conn->query('COMMIT;');
					}
					else
					{
					   $array[$name]="Sorry, Available Slots Got Completely Booked. Please Try Another Day";
					   $conn->query('ROLLBACK;');
					}
				}
				elseif ($f==2)
				{
					$conn->query('BEGIN;');
					$res1=$conn->query("SELECT first_dose_remaining+second_dose_remaining as Balance FROM tbl_balance WHERE vd_id=$vd_id AND second_dose_remaining>0 for UPDATE");
					if ($row=$res1->fetch_assoc())
					{
						$balance=$row['Balance'];
						$res2=$conn->query("UPDATE tbl_balance SET second_dose_remaining=second_dose_remaining-1 WHERE vd_id=$vd_id");
						if($res2)
						{
							$token_no=$count-$balance+1;
							if ($token_no==1)
							{
								$res3=$conn->query("SELECT starting_time FROM tbl_vaccination_day WHERE vd_id=$vd_id");
								if($row3=$res3->fetch_assoc())
								{
									$time=date('H:i:s',strtotime($row3['starting_time']));
								}
							}
							else
							{
								$res3=$conn->query("SELECT time FROM tbl_slot WHERE token_no=$token_no-1 AND vd_id=$vd_id");
								if ($row3=$res3->fetch_assoc())
								{
									$time=$row3['time'];
									if ($token_no%10==1)
									{
										$time=date('H:i:s',strtotime('+15 minutes',strtotime($time)));
									}
								}
							}

							$conn->query("UPDATE tbl_vaccination_day SET status=0 WHERE vd_id=(SELECT vd_id FROM tbl_balance WHERE vd_id=$vd_id AND first_dose_remaining=0 AND second_dose_remaining=0)");

							$res4=$conn->query("SELECT public_id FROM tbl_public WHERE name='$name' AND age=$age AND mobile_no='$phn_no'");
							if($row4=$res4->fetch_assoc())
							{
								$public_id=intval($row4['public_id']);
							}
							else
							{
								$query1=$conn->query("INSERT INTO tbl_public(aadhar,name,age,mobile_no,address,panchayath_id) VALUES ('$aadhaar','$name',$age,'$phn_no','$address',$panchayat)");
								$res4=$conn->query("SELECT public_id FROM tbl_public WHERE name='$name' AND age=$age AND mobile_no='$phn_no'");
							   if($row4=$res4->fetch_assoc())
							   {
									$public_id=intval($row4['public_id']);
							   }
							}
							$query2=$conn->query("INSERT INTO tbl_slot (token_no,public_id,vd_id,dose_no,time) VALUES ($token_no,$public_id,$vd_id,$f,'$time')");
							if ($query2)
							{
								$flag=1;
								$array[$name]="Your Covid Vaccine Registration is Successful";
								$date=date('d/m/Y',strtotime($date));
								$res5=$conn->query("SELECT vaccine_name FROM tbl_vaccine WHERE vaccine_id=$vaccine_id");
								if ($row5=$res5->fetch_assoc())
								{
									$vaccine_name=$row5['vaccine_name'];
								}
								$content.='<tr><td>'.$token_no.' - '.$vaccine_name.'</td><td>'.$name.'</td><td>'.$age.'</td><td>'.$phn_no.'</td><td>'.$f.'</td><td>'.$date.' '.$time.'</td></tr>';
							}
						}
						$conn->query('COMMIT;');
					}
					else
					{
						$array[$name]="Sorry, Available Slots Got Completely Booked. Please Try Another Day";
						$conn->query('ROLLBACK;');
					}
				}
			}
			else
			{
				$array[$name]="Already Registered";
			}
		}
		else
		{
		   $array[$name]="Sorry, Available Slots Got Completely Booked. Please Try Another Day";
		}
	}

	if(isset($_POST['check4'])&&$_POST['check4']=='check')
	{
		$vaccine_id=$_POST['vaccine4'];
		$name=$_POST['name4'];
		$age=intval($_POST['age4']);
		$aadhaar=$_POST['aadhaar_no4'];
		$address=$_POST['address4'];
		$panchayat=intval($_POST['panchayat4']);
		$phn_no=$_POST['phn_no4'];

		$res=$conn->query("SELECT v.count,v.date,v.vd_id FROM tbl_vaccination_day v WHERE v.status=1 and v.vaccine_id=$vaccine_id and v.date=(SELECT v.date from tbl_vaccination_day v where v.status=1 and v.vaccine_id=$vaccine_id ORDER BY v.date ASC LIMIT 1)");
		if ($res->num_rows)
		{
			$count=0;
			if ($row=$res->fetch_assoc())
			{
				$count=$row['count'];
				$date=$row['date'];
				$vd_id=intval($row['vd_id']);
			}

			$result=$conn->query("SELECT p.public_id FROM tbl_public p INNER JOIN tbl_slot s USING(public_id) WHERE p.name='$name' AND p.age=$age AND p.mobile_no='$phn_no' AND s.vd_id IN (SELECT v.vd_id FROM tbl_vaccination_day v WHERE v.date='$date')");
			if ($result->num_rows==0)
			{
				if($_POST['dose4']=='dose1')
					$f=1;
				else
					$f=2;
				if ($f==1)
				{
					$conn->query('BEGIN;');
					$res1=$conn->query("SELECT first_dose_remaining+second_dose_remaining as Balance FROM tbl_balance WHERE vd_id=$vd_id AND first_dose_remaining>0 for UPDATE");
					if ($row=$res1->fetch_assoc())
					{
						$balance=$row['Balance'];
						$res2=$conn->query("UPDATE tbl_balance SET first_dose_remaining=first_dose_remaining-1 WHERE vd_id=$vd_id");
						if($res2)
						{
							$token_no=$count-$balance+1;
							if ($token_no==1)
							{
								$res3=$conn->query("SELECT starting_time FROM tbl_vaccination_day WHERE vd_id=$vd_id");
								if($row3=$res3->fetch_assoc())
									$time=date('H:i:s',strtotime($row3['starting_time']));
							}
							else
							{
								$res3=$conn->query("SELECT time FROM tbl_slot WHERE token_no=$token_no-1 AND vd_id=$vd_id");
								if ($row3=$res3->fetch_assoc())
								{
									$time=$row3['time'];
									if ($token_no%10==1)
										$time=date('H:i:s',strtotime('+15 minutes',strtotime($time)));
								}
							}

							$conn->query("UPDATE tbl_vaccination_day SET status=0 WHERE vd_id=(SELECT vd_id FROM tbl_balance WHERE vd_id=$vd_id AND first_dose_remaining=0 AND second_dose_remaining=0)");

							$query1=$conn->query("INSERT INTO tbl_public(aadhar,name,age,mobile_no,address,panchayath_id) VALUES ('$aadhaar','$name',$age,'$phn_no','$address',$panchayat)");
							$res4=$conn->query("SELECT public_id FROM tbl_public WHERE name='$name' AND age=$age AND mobile_no='$phn_no'");
							if($row4=$res4->fetch_assoc())
								$public_id=intval($row4['public_id']);
							$query2=$conn->query("INSERT INTO tbl_slot (token_no,public_id,vd_id,dose_no,time) VALUES ($token_no,$public_id,$vd_id,$f,'$time')");
							if ($query2)
							{
								$flag=1;
								$array[$name]="Your Covid Vaccine Registration is Successful";
								$date=date('d/m/Y',strtotime($date));
								$res5=$conn->query("SELECT vaccine_name FROM tbl_vaccine WHERE vaccine_id=$vaccine_id");
								if ($row5=$res5->fetch_assoc())
								{
									$vaccine_name=$row5['vaccine_name'];
								}
								$content.='<tr><td>'.$token_no.' - '.$vaccine_name.'</td><td>'.$name.'</td><td>'.$age.'</td><td>'.$phn_no.'</td><td>'.$f.'</td><td>'.$date.' '.$time.'</td></tr>';
							}
						}
						$conn->query('COMMIT;');
					}
					else
					{
					  $array[$name]="Sorry, Available Slots Got Completely Booked. Please Try Another Day";
					   $conn->query('ROLLBACK;');
					}
				}
				elseif ($f==2)
				{
					$conn->query('BEGIN;');
					$res1=$conn->query("SELECT first_dose_remaining+second_dose_remaining as Balance FROM tbl_balance WHERE vd_id=$vd_id AND second_dose_remaining>0 for UPDATE");
					if ($row=$res1->fetch_assoc())
					{
						$balance=$row['Balance'];
						$res2=$conn->query("UPDATE tbl_balance SET second_dose_remaining=second_dose_remaining-1 WHERE vd_id=$vd_id");
						if($res2)
						{
							$token_no=$count-$balance+1;
							if ($token_no==1)
							{
								$res3=$conn->query("SELECT starting_time FROM tbl_vaccination_day WHERE vd_id=$vd_id");
								if($row3=$res3->fetch_assoc())
								{
									$time=date('H:i:s',strtotime($row3['starting_time']));
								}
							}
							else
							{
								$res3=$conn->query("SELECT time FROM tbl_slot WHERE token_no=$token_no-1 AND vd_id=$vd_id");
								if ($row3=$res3->fetch_assoc())
								{
									$time=$row3['time'];
									if ($token_no%10==1)
									{
										$time=date('H:i:s',strtotime('+15 minutes',strtotime($time)));
									}
								}
							}

							$conn->query("UPDATE tbl_vaccination_day SET status=0 WHERE vd_id=(SELECT vd_id FROM tbl_balance WHERE vd_id=$vd_id AND first_dose_remaining=0 AND second_dose_remaining=0)");

							$res4=$conn->query("SELECT public_id FROM tbl_public WHERE name='$name' AND age=$age AND mobile_no='$phn_no'");
							if($row4=$res4->fetch_assoc())
							{
								$public_id=intval($row4['public_id']);
							}
							else
							{
								$query1=$conn->query("INSERT INTO tbl_public(aadhar,name,age,mobile_no,address,panchayath_id) VALUES ('$aadhaar','$name',$age,'$phn_no','$address',$panchayat)");
								$res4=$conn->query("SELECT public_id FROM tbl_public WHERE name='$name' AND age=$age AND mobile_no='$phn_no'");
							   if($row4=$res4->fetch_assoc())
							   {
									$public_id=intval($row4['public_id']);
							   }
							}
							$query2=$conn->query("INSERT INTO tbl_slot (token_no,public_id,vd_id,dose_no,time) VALUES ($token_no,$public_id,$vd_id,$f,'$time')");
							if ($query2)
							{
								$flag=1;
								$array[$name]="Your Covid Vaccine Registration is Successful";
								$date=date('d/m/Y',strtotime($date));
								$res5=$conn->query("SELECT vaccine_name FROM tbl_vaccine WHERE vaccine_id=$vaccine_id");
								if ($row5=$res5->fetch_assoc())
								{
									$vaccine_name=$row5['vaccine_name'];
								}
								$content.='<tr><td>'.$token_no.' - '.$vaccine_name.'</td><td>'.$name.'</td><td>'.$age.'</td><td>'.$phn_no.'</td><td>'.$f.'</td><td>'.$date.' '.$time.'</td></tr>';
							}
						}
						$conn->query('COMMIT;');
					}
					else
					{
						$array[$name]="Sorry, Available Slots Got Completely Booked. Please Try Another Day";
						$conn->query('ROLLBACK;');
					}
				}
			}
			else
			{
				$array[$name]="Already Registered";
			}
		}
		else
		{
		   $array[$name]="Sorry, Available Slots Got Completely Booked. Please Try Another Day";
		}
	}

	$content.='</table></body></html>';
    $_SESSION['content']=$content;

    echo '<body background="form_bg.jpg" text="white"><div><h1 align="center">Registration Status</h1><br><h3>Please Make Sure You Download the Registration Slip!!!</h3><br><br><table border="1"><tr><th>Name</th><th>Registration Status</th></tr>';
    foreach ($array as $key => $value)
    {
   		echo "<tr><td>".$key."</td><td>".$value."</td></tr>";
	}
    echo '</table><br><br><h4><i>Click <a href="index.php">Here</a> to Return to Home Page</i></h4></body></div>';

    if ($flag==1)
    {
    ?>
        <script type="text/javascript">
            location.replace("registration_slip.php");
        </script>
    <?php

    }
    else
    {
    	session_unset();
    	session_destroy();
    }
}
?>
