<?php
include "../connection.php";
session_start();
if(!isset($_SESSION['user_type']))
{

 	echo "<script>alert('Session Expired');</script>";
 	echo '<script type="text/javascript">
 			location.replace("index.php");
 			</script>';
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Registration</title>
	<link rel="stylesheet" type="text/css" href="public_css.css">
	<script src="https://code.jquery.com/jquery-latest.js"></script>
	<script>

        function disableBack() { window.history.forward(); }
        setTimeout("disableBack()", 0);
        window.onunload = function () { null };

		$(document).ready(function(){
		     $("#balance").load("load_balance.php");
		     setInterval(function() {
		          $("#balance").load("load_balance.php");
		          },1000);});
	</script>
</head>
<body background="form_bg.jpg" text="white">
  <h1 align="center" style="font-family: 'times new roman'">TALUK HEAD QUARTERS HOSPITAL PAMPADY<br>KOTTAYAM</h1>
  <div>
    <img class="img_container" id="logo" src="govt_public.png" alt="Kerala Logo" >
  </div>
	<h2 align="center">Registration Form</h2>
	<div id="balance" style="position: sticky; top: 0; background-color: #E00000; text-align: center; color: white;"></div>
		
	<br><br>
	<div class="form">
		<form method="post" action="Public_Submit.php">
			<div width="100%">
				<table>
					<tr style="height: 40px;">
						<td colspan="2">
							<label for="dose1_1">Dose 1</label>
							<input type="radio" name="dose1" id="dose1_1" value="dose1" required>
							&emsp;						
							<label for="dose2_1">Dose 2</label>
							<input type="radio" name="dose1" id="dose2_1" value="dose2">							
					    </td>
					</tr> 
					<tr>
						<td>
							<label for="vaccine1">Name of Vaccine <span>*</span></label>		
						</td>
						<td>
							<select name="vaccine1" id="vaccine1" required>
					      	<option value="" disabled selected>-Select-</option>
							    <?php
							      $result=$conn->query("SELECT v.vaccine_id,v.vaccine_name FROM tbl_vaccine v INNER JOIN tbl_vaccination_day vd USING(vaccine_id) WHERE vd.status=1 and vd.date=(SELECT vd.date FROM tbl_vaccination_day vd WHERE vd.status=1 ORDER BY vd.date ASC LIMIT 1)");
							      if($result->num_rows>0)
                      while($row=$result->fetch_assoc())
                        echo '<option value="'.$row['vaccine_id'].'">'.$row['vaccine_name'].'</option>';
                  ?>
						    </select>		
						</td>
					</tr>
					<tr>
						<td>
							<label for="name1">Name <span>*</span></label>
						</td>
						<td>
							<input type="text" name="name1" id="name1" pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable" required>
						</td>
					</tr>
					<tr>
						<td>
							<label for="age1">Age <span>*</span></label>
						</td>
						<td>
							<input type="number" name="age1" id="age1" minlength="2" min="0" required>
						</td>
					</tr>
					<tr> 
						<td>
							<label for="aadhaar_no1">Aadhaar Number</label>
						</td>
						<td>
							<input type="text" name="aadhaar_no1" id="aadhaar_no1"  pattern="[0-9]{12}" title="please enter a valid Aadhaar number">
						</td>
					</tr>
					<tr>
						<td>
						  <label for="address1">Address</label>
						</td>
						<td>
							<textarea name="address1" id="address1"></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<label for="panchayat1">Panchayat <span>*</span></label>
						</td>
						<td>
							<select name="panchayat1" id="panchayat1" required>
					      	<option value="" disabled selected>-Select-</option>
							    <?php
							      $result=$conn->query("SELECT panchayath_id,panchayath_name FROM tbl_panchayath WHERE status=1");
							      if($result->num_rows>0)
                      while($row=$result->fetch_assoc())
                        echo '<option value="'.$row['panchayath_id'].'">'.$row['panchayath_name'].'</option>';
                  ?>
						    </select>		
						</td>
					</tr>
					<tr>
						<td>
							<label for="phn_no1">Phone Number <span>*</span></label>
						</td>
						<td>
							<input type="text" name="phn_no1" id="phn_no1" pattern="[5-9]{1}[0-9]{9}" title="please enter a 10 digit valid phone number" value="" required>
						</td>
					</tr>
				</table>
				<br>
				<input type="button" style="background-color: silver; width: 100%; height: 30px; border: none; font-weight: bold;" value="+ Add Member" id="add2" onclick="show2()">
			</div>
			<div id="div2"  width="100%" style="display: none">
				<hr>
				<input type="radio" name="check2" id="check2" value="check" hidden>
				<table>
					<tr>
						<td colspan="2">
							<label for="dose1_2">Dose 1</label>
							<input type="radio" name="dose2" id="dose1_2" value="dose1">
							&emsp;						
							<label for="dose2_2">Dose 2</label>
							<input type="radio" name="dose2" id="dose2_2" value="dose2">
						</td>
					</tr>
					<tr>
						<td>
							<label for="vaccine2">Name of Vaccine <span>*</span></label>		
						</td>
						<td>
							<select name="vaccine2" id="vaccine2">
					      	<option value="" disabled selected>-Select-</option>
							    <?php
							      $result=$conn->query("SELECT v.vaccine_id,v.vaccine_name FROM tbl_vaccine v INNER JOIN tbl_vaccination_day vd USING(vaccine_id) WHERE vd.status=1 and vd.date=(SELECT vd.date FROM tbl_vaccination_day vd WHERE vd.status=1 ORDER BY vd.date ASC LIMIT 1)");
							      if($result->num_rows>0)
                      while($row=$result->fetch_assoc())
                        echo '<option value="'.$row['vaccine_id'].'">'.$row['vaccine_name'].'</option>';
                  ?>
						    </select>		
						</td>
					</tr>
					<tr>
						<td>
							<label for="name2">Name <span>*</span></label>
						</td>
						<td>
							<input type="text" name="name2" id="name2" pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable">
						</td>
					</tr>
					<tr>
						<td>
							<label for="age2">Age <span>*</span></label>
						</td>
						<td>
							<input type="number" minlength="2" name="age2" id="age2">
						</td>
					</tr>
					<tr>
						<td>
							<label for="aadhaar_no2">Aadhaar Number</label>
						</td>
						<td>
							<input type="text" name="aadhaar_no2" id="aadhaar_no2"  pattern="[0-9]{12}" title="please enter a valid Aadhaar number">
						</td>
					</tr>
					<tr>
						<td>
						  <label for="address2">Address</label>
						</td>
						<td>
							<textarea name="address2" id="address2"></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<label for="panchayat2">Panchayat <span>*</span></label>
						</td>
						<td>
							<select name="panchayat2" id="panchayat2">
					      	<option value="" disabled selected>-Select-</option>
							    <?php
							      $result=$conn->query("SELECT panchayath_id,panchayath_name FROM tbl_panchayath WHERE status=1");
							      if($result->num_rows>0)
                      while($row=$result->fetch_assoc())
                        echo '<option value="'.$row['panchayath_id'].'">'.$row['panchayath_name'].'</option>';
                  ?>
						    </select>		
						</td>
					</tr>
					<tr>
						<td>
							<label for="phn_no2">Phone Number <span>*</span></label>
						</td>
						<td>
							<input type="text" name="phn_no2" id="phn_no2" pattern="[5-9]{1}[0-9]{9}" title="please enter a 10 digit valid phone number">
						</td>
					</tr>
				</table>
				<input type="button" class="rm" value="- Remove" id="remove2" onclick="cancel2()">
				<br><br>
				<input type="button" style="background-color: silver; width: 100%; height: 30px; border: none; font-weight: bold;" value="+ Add Member" id="add3" onclick="show3()">
			</div>
			<div id="div3" width="100%"  style="display: none">
				<hr>
				<input type="radio" name="check3" id="check3" value="check" hidden>
				<table>
					<tr>
						<td colspan="2">
							<label for="dose1_3">Dose 1</label>
							<input type="radio" name="dose3" id="dose1_3" value="dose1">
							&emsp;						
							<label for="dose2_3">Dose 2</label>
							<input type="radio" name="dose3" id="dose2_3" value="dose2">
						</td>
					</tr>
					<tr>
						<td>
							<label for="vaccine3">Name of Vaccine <span>*</span></label>		
						</td>
						<td>
							<select name="vaccine3" id="vaccine3">
					      	<option value="" disabled selected>-Select-</option>
							    <?php
							      $result=$conn->query("SELECT v.vaccine_id,v.vaccine_name FROM tbl_vaccine v INNER JOIN tbl_vaccination_day vd USING(vaccine_id) WHERE vd.status=1 and vd.date=(SELECT vd.date FROM tbl_vaccination_day vd WHERE vd.status=1 ORDER BY vd.date ASC LIMIT 1)");
							      if($result->num_rows>0)
                      while($row=$result->fetch_assoc())
                        echo '<option value="'.$row['vaccine_id'].'">'.$row['vaccine_name'].'</option>';
                  ?>
						    </select>		
						</td>
					</tr>
					<tr>
						<td>
							<label for="name3">Name <span>*</span></label>
						</td>
						<td>
							<input type="text" name="name3" id="name3" pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable">
						</td>
					</tr>
					<tr>
						<td>
							<label for="age3">Age <span>*</span></label>
						</td>
						<td>
							<input type="number" minlength="2" name="age3" id="age3">
						</td>
					</tr>
					<tr>
						<td>
							<label for="aadhaar_no3">Aadhaar Number</label>
						</td>
						<td>
							<input type="text" name="aadhaar_no3" id="aadhaar_no3"  pattern="[0-9]{12}" title="please enter a valid Aadhaar number">
						</td>
					</tr>
					<tr>
						<td>
						  <label for="address3">Address</label>
						</td>
						<td>
							<textarea name="address3" id="address3"></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<label for="panchayat3">Panchayat <span>*</span></label>
						</td>
						<td>
							<select name="panchayat3" id="panchayat3">
					      	<option value="" disabled selected>-Select-</option>
							    <?php
							      $result=$conn->query("SELECT panchayath_id,panchayath_name FROM tbl_panchayath WHERE status=1");
							      if($result->num_rows>0)
                      while($row=$result->fetch_assoc())
                        echo '<option value="'.$row['panchayath_id'].'">'.$row['panchayath_name'].'</option>';
                  ?>
						    </select>		
						</td>
					</tr>
					<tr>
						<td>
							<label for="phn_no3">Phone Number <span>*</span></label>
						</td>
						<td>
							<input type="text" name="phn_no3" id="phn_no3" pattern="[5-9]{1}[0-9]{9}" title="please enter a 10 digit valid phone number">
						</td>
					</tr>
				</table>
				<input type="button" class="rm" value="- Remove" id="remove3" onclick="cancel3()">
				<br><br>
				<input type="button" style="background-color: silver; width: 100%; height: 30px; border: none; font-weight: bold;" value="+ Add Member" id="add4" onclick="show4()">
			</div>
			<div id="div4" width="100%" style="display: none">
				<hr>
				<input type="radio" name="check4" id="check4" value="check" hidden>
				<table>
					<tr>
						<td colspan="2">
							<label for="dose1_4">Dose 1</label>
							<input type="radio" name="dose4" id="dose1_4" value="dose1">
							&emsp;						
							<label for="dose2_4">Dose 2</label>
							<input type="radio" name="dose4" id="dose2_4" value="dose2">
						</td>
					</tr>
					<tr>
						<td>
							<label for="vaccine4">Name of Vaccine <span>*</span></label>		
						</td>
						<td>
							<select name="vaccine4" id="vaccine4">
					      	<option value="" disabled selected>-Select-</option>
							    <?php
							      $result=$conn->query("SELECT v.vaccine_id,v.vaccine_name FROM tbl_vaccine v INNER JOIN tbl_vaccination_day vd USING(vaccine_id) WHERE vd.status=1 and vd.date=(SELECT vd.date FROM tbl_vaccination_day vd WHERE vd.status=1 ORDER BY vd.date ASC LIMIT 1)");
							      if($result->num_rows>0)
                      while($row=$result->fetch_assoc())
                        echo '<option value="'.$row['vaccine_id'].'">'.$row['vaccine_name'].'</option>';
                  ?>
						    </select>		
						</td>
					</tr>
					<tr>
						<td>
							<label for="name4">Name <span>*</span></label>
						</td>
						<td>
							<input type="text" name="name4" id="name4" pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable">
						</td>
					</tr>
					<tr>
						<td>
							<label for="age4">Age <span>*</span></label>
						</td>
						<td>
							<input type="number" minlength="2" name="age4" id="age4">
						</td>
					</tr>
					<tr>
						<td>
							<label for="aadhaar_no4">Aadhaar Number</label>
						</td>
						<td>
							<input type="text" name="aadhaar_no4" id="aadhaar_no4"  pattern="[0-9]{12}" title="please enter a valid Aadhaar number">
						</td>
					</tr>
					<tr>
						<td>
						  <label for="address4">Address</label>
						</td>
						<td>
							<textarea name="address4" id="address4"></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<label for="panchayat4">Panchayat <span>*</span></label>
						</td>
						<td>
							<select name="panchayat4" id="panchayat4">
					      	<option value="" disabled selected>-Select-</option>
							    <?php
							      $result=$conn->query("SELECT panchayath_id,panchayath_name FROM tbl_panchayath WHERE status=1");
							      if($result->num_rows>0)
                      while($row=$result->fetch_assoc())
                        echo '<option value="'.$row['panchayath_id'].'">'.$row['panchayath_name'].'</option>';
                  ?>
						    </select>		
						</td>
					</tr>
					<tr>
						<td>
							<label for="phn_no4">Phone Number <span>*</span></label>
						</td>
						<td>
							<input type="text" name="phn_no4" id="phn_no4" pattern="[5-9]{1}[0-9]{9}" title="please enter a 10 digit valid phone number">
						</td>
					</tr>
				</table>
				<input type="button" class="rm" value="- Remove" id="remove4" onclick="cancel4()">
			</div>
			<br>
			<div class="submit_center" width="100%">
				<button type="submit" name="submit" id="submit" class="submit" onclick='return confirm("Once You Submit, No More Changes are Allowed. Are You Sure to Proceed?")'>Submit</button>
			</div>
		</form>
	</div>

	<script type="text/javascript">
		function show2() 
		{
			document.getElementById("div2").style.display = "block";
			document.getElementById("add2").hidden = true;
			document.getElementById("vaccine2").required=true;
			document.getElementById("name2").required = true;
			document.getElementById("phn_no2").required=true;
			document.getElementById("age2").required=true;
			document.getElementById("dose1_2").required=true;
			document.getElementById("panchayat2").required=true;		

			document.getElementById("check2").checked=true;
		}
		function show3() 
		{			
			document.getElementById("div3").style.display = "block";
			document.getElementById("add3").hidden = true;
			document.getElementById("vaccine3").required=true;
			document.getElementById("name3").required = true;
			document.getElementById("phn_no3").required=true;
			document.getElementById("age3").required=true;
			document.getElementById("dose1_3").required=true;
			document.getElementById("panchayat3").required=true;
			
			document.getElementById("check3").checked=true;
		}
		function show4() 
		{			
			document.getElementById("div4").style.display = "block";
			document.getElementById("add4").hidden = true;
			document.getElementById("vaccine4").required=true;
			document.getElementById("name4").required = true;
			document.getElementById("phn_no4").required=true;
			document.getElementById("age4").required=true;
			document.getElementById("dose1_4").required=true;
			document.getElementById("panchayat4").required=true;

			document.getElementById("check4").checked=true;
		}
		function cancel2() 
		{
			document.getElementById("div2").style.display = "none";
			document.getElementById("add2").hidden=false;
			document.getElementById("vaccine2").required=false;
			document.getElementById("name2").required = false;
			document.getElementById("phn_no2").required=false;
			document.getElementById("age2").required=false;
			document.getElementById("dose1_2").required=false;
			document.getElementById("panchayat2").required=false;

			document.getElementById("check2").checked=false;
		}
		function cancel3() 
		{
			document.getElementById("div3").style.display = "none";
			document.getElementById("add3").hidden = false;
			document.getElementById('dose1_3').required=false;
			document.getElementById("vaccine3").required=false;
			document.getElementById("name3").required = false;
			document.getElementById("phn_no3").required=false;
			document.getElementById("age3").required=false;
			document.getElementById("panchayat3").required=false;

			document.getElementById("check3").checked=false;
		}
		function cancel4() 
		{
			document.getElementById("div4").style.display = "none";
			document.getElementById("add4").hidden = false;
			document.getElementById('dose1_4').required=false;
			document.getElementById("vaccine4").required=false;
			document.getElementById("name4").required = false;
			document.getElementById("phn_no4").required=false;
			document.getElementById("age4").required=false;
			document.getElementById("panchayat4").required=false;

			document.getElementById("check4").checked=false;
		}
	</script>				
</body>
</html>

