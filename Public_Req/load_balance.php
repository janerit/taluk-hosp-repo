<script type="text/javascript">
	function disableBack() { window.history.forward(); }
        setTimeout("disableBack()", 0);
        window.onunload = function () { null };
</script>

<?php
session_start();
if(!isset($_SESSION['user_type']))
{

 	echo "<script>alert('Session Expired');</script>";
 	echo '<script type="text/javascript">
 			location.replace("index.php");
 			</script>';
}
include "../connection.php";
date_default_timezone_set("Asia/Kolkata");
echo date("l jS \of F Y h:i:s A");
$res=$conn->query("SELECT v.vaccine_name,b.first_dose_remaining,b.second_dose_remaining,vd.date FROM tbl_balance b INNER JOIN tbl_vaccination_day vd USING(vd_id) INNER JOIN tbl_vaccine v USING(vaccine_id) where vd.status=1 and vd.date=(SELECT vd.date from tbl_vaccination_day vd where vd.status=1 ORDER BY vd.date ASC LIMIT 1)");
if ($res->num_rows>0) 
{
	while ($row=$res->fetch_assoc())
	{ 
		$date = date('d/m/Y',strtotime($row['date']));
		echo '<br><br><b>'.$row['vaccine_name'].'  remaining on '.$date.' :  First Dose : '.$row['first_dose_remaining'].' | Second Dose : '.$row['second_dose_remaining'];
	}	
}
else
{
	echo "<br><br>Oops!!! You were Late... Come Back Later...";
}
$conn->close();
?> 