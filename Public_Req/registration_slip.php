<?php
require_once('../TCPDF/examples/tcpdf_include.php');
require_once('../TCPDF/tcpdf.php');
session_start();
$content=$_SESSION['content'];

function generatePDF($output, $content)
{
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->AddPage();
    $pdf->SetFont('Times','',10); 
    $pdf->WriteHTML($content);
    $pdf->Output($output, "D");
}

generatePDF("Registration Slip.pdf", $content);

session_unset();
session_destroy();
?>