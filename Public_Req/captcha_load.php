<?php
session_start();

if(isset($_POST["submit"])) {

$captcha = $_POST["captcha"];

$captchaUser = filter_var($_POST["captcha"], FILTER_SANITIZE_STRING);

if(empty($captcha)) {
  echo "<script>alert('Please enter the captcha.');</script>";
  echo '<script type="text/javascript">
  location.replace("index.php");
  </script>';
}
else if($_SESSION['CAPTCHA_CODE'] == $captchaUser){
  $_SESSION["user_type"] = "public";
   echo '<script type="text/javascript">
   location.replace("Public_Registration.php");
   </script>';
} else {
  echo "<script>alert('Captcha is invalid. Try Again !!');</script>";
  echo '<script type="text/javascript">
  location.replace("index.php");
  </script>';

      }
    }

?>
