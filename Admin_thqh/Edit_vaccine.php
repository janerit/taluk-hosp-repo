<?php
	session_start();
	if(!isset($_SESSION['login_user']))
	{
		echo "<script>alert('Session Expired');</script>";
		echo '<script type="text/javascript">
				location.replace("../index.php");
				</script>';
	}


	//including database connection file
	include "../connection.php" ;
	########################  PAGE FOR EDIT/REMOVE VACCINE  ########################

		if(isset($_GET['vaccine_edt_id']))
		{

			$vaccine_id=$_GET['vaccine_edt_id'];

			$records=mysqli_query($conn,"select * from tbl_vaccine where vaccine_id=$vaccine_id");
			$data = mysqli_fetch_assoc($records);

?>


			<html>
				<head>
					<title>Edit Vaccine</title>

					<!--styles file including-->
					<link rel="stylesheet" href="../ktfo_css.css">
				</head>

				<body>
					<!--Header-->
					<div>
					<?php
					 include "../menu_for_folder.php" ;
					 ?>
					</div>

					<!--form to add new vaccine-->
					<h3 align="center">Edit vaccine Name</h3>
					<div class="form" >
						<form id="vaccine_add" name="vaccine_add" method="post" action="">
						  <table>
							  <center>
								<tr>
								  <td>Vaccine Name<span> * </span></td>
								  <td><input type="text" name="v_name" value="<?php echo $data['vaccine_name']; ?>" required="required"/></td>
								</tr>

								<tr>
								  <td colspan="2" align="center"><button type="cancel">CANCEL</button>
									 <button type="submit" name="edit">Edit</button></td>
								</tr>
							  </center>
							</table>
						</form>
					</div>
				</body>
			</html>
<?php

			if(isset($_POST['edit']))
			{
				//UPDATE table tbl_vaccine
				$v_name=$_POST['v_name'];

				if(mysqli_query($conn,"UPDATE tbl_vaccine SET vaccine_name='$v_name' WHERE vaccine_id=$vaccine_id"))
				{
					echo "<script>alert('Vaccine Name Updated');</script>";
					echo '<script type="text/javascript">
						location.replace("Add_vaccine.php");
						</script>';
				}
				else
				{
					echo "<script>alert('Updation failed');</script>";
					echo '<script type="text/javascript">
						location.replace("Add_vaccine.php");
						</script>';
				}
			}

		}

?>
