<?php
 session_start();
 if(!isset($_SESSION['login_user']))
 {
 	echo "<script>alert('Session Expired');</script>";
 	echo '<script type="text/javascript">
 			location.replace("../index.php");
 			</script>';
 }
//Including database connection file
include "../connection.php" ;
?>
<!DOCTYPE html>
<html>
<head>
<title>Admin_Section_Add</title>

<!--styles file including-->
<link rel="stylesheet" href="../ktfo_css.css">
<link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet"
    type="text/css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script language="javascript">
    $(document).ready(function () {
        $("#txtdate").datepicker({
            minDate: 0
        });
    });
</script>
</head>

<body>

<div>
	<?php
	include "../menu_for_folder.php" ;
	 ?>
</div>
<!--form to add new section-->
<h2 align="center">Vaccine Schedule</h2>

<div class="form" >
<form id="section_add" name="section_add" method="post" action="" onSubmit="return submit_form()">
  <table>
    <tr>
      <td>Date<span> * </span></td>
      <td><input id="txtdate" type="text" name="date" required="required"></td>
    </tr>
	<tr>
	  <td>Vaccine Name<span> *</td>
			<td><select name="vaccine_id">
			<?php
			$records = mysqli_query($conn,"select * from tbl_vaccine");
	while($data = mysqli_fetch_array($records))
	{
			 ?>

	<option value="<?php echo $data['vaccine_id']; ?> "><?php echo $data['vaccine_name']; ?></option>
	<?php
}
	 ?>
</select></td></tr>


	<tr>
	  <td>Count<span> *</td>
      <td><input type="text"  name="count" required="required" /></td>
    </tr>
		<tr>
			<td>Starting Time<span> *</td>
				<td><input type="time"  name="starting_time" required="required" /></td>
			  <td>Ending Time<span> *</td>
		      <td><input type="time"  name="ending_time" required="required" /></td>
		    </tr>
				<br><br>
    <tr>
      <td colspan="2" align="center"><button type="cancel" >CANCEL</button>
	  	 <button type="submit" name="add">ADD</button></td>
    </tr>
  </table>
</form>
</div>
<div style="bottom:0; width:100%;">
<?php
//including footer file
include "../Footer.php";
?>
</body>
</html>
<?php
 if(isset($_POST['add']))
 {
	$date=date("Y-m-d",strtotime($_POST['date']));
	$count=$_POST['count'];
	$vaccine_id=$_POST['vaccine_id'];
	$starting_time=date("Y-m-d H:i:s",strtotime($_POST['date'].$_POST['starting_time']));
	$ending_time=date("Y-m-d H:i:s",strtotime($_POST['date'].$_POST['ending_time']));

	//inserting the data collected from the form to tbl_vaccination_day
  $sql1="insert into tbl_vaccination_day(date,count,vaccine_id,starting_time,ending_time,status)values('$date',$count,$vaccine_id,'$starting_time','$ending_time',1)";
  if(mysqli_query($conn,$sql1))
  {
    $sql_vdid="SELECT vd_id FROM `tbl_vaccination_day` WHERE date ='$date' AND count=$count AND vaccine_id=$vaccine_id AND starting_time='$starting_time' AND ending_time='$ending_time'";
    $result = $conn->query($sql_vdid);
    if($result->num_rows)
  	{
        $row = mysqli_fetch_assoc($result);
        $vdid = $row['vd_id'];
        echo $vdid."<br>";
    }
  }
  //Fetching First Dose Percentage
  $sql_percentage="SELECT firstdose_percent FROM `tbl_count_percentage` ORDER BY percentage_id DESC LIMIT 1";
  $result = $conn->query($sql_percentage);
	if($result->num_rows)
	{
      $row = mysqli_fetch_assoc($result);
      $count_percentage = $row['firstdose_percent'];
      //First and second dose count
      $first_dose_count=ceil(($count*$count_percentage)/100);
      $second_dose_count=$count-$first_dose_count;
      //Inserting to tbl_balance the remaining vaccine doses
      $sql2="insert into tbl_balance(vd_id,first_dose_remaining,second_dose_remaining)values($vdid,$first_dose_count,$second_dose_count)";
    	if(mysqli_query($conn,$sql2))
    	 	{
          echo "<script>alert('Added Successfully');</script>";
          echo '<script type="text/javascript">
            location.replace("home.php");
            </script>';
		     }
	else
		{
	?>
  	<script> alert("failed");</script>
<?php
		}
}
}
?>
