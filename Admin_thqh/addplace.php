<?php
 session_start();
 if(!isset($_SESSION['login_user']))
{
 	echo "<script>alert('Session Expired');</script>";
 	echo '<script type="text/javascript">
			location.replace("../index.php");
 			</script>';
 }
//Including database connection file
include "../connection.php" ;
?>
<!DOCTYPE html>
<html>
<head>
<title>Add Panchayath Name</title>

<!--styles file including-->
<!-- <link rel="stylesheet" href="../ktfo_css.css"> -->
	<link rel="stylesheet" type="text/css" href="../css.css">
</head>

<body>

<div>
<?php
include "../menu_for_folder.php";
?>
</div>

<!--form to add new panchayath_Name-->
<h3 align="center">PANCHAYATH NAME</h3>
<div class="form" >
<form id="section_add" name="section_add" method="post" action>
  <table>
    <tr>
      <td>Panchayath Name<span> * </span></td>
      <td><input type="text" name="panchayath_name" required="required"/></td>
    </tr>
    <tr>
      <td colspan="2" align="center"><button type="reset">RESET</button>
	  	 <button type="submit" name="add">ADD</button></td>
    </tr>
  </table>
</form>

<?php

if(isset($_POST['add']))
{

 $panchayath_name=$_POST['panchayath_name'];

 $result=mysqli_query($conn,"select * from tbl_panchayath where panchayath_name='$panchayath_name'");
 if($result->num_rows>0)
   {

    $row=$result->fetch_assoc();
    if($row['status']==0)
     {
      $panchayath_id=$row['panchayath_id'];
      $res=mysqli_query($conn,"UPDATE tbl_panchayath SET status=1 WHERE panchayath_id=$panchayath_id");
      echo "<script>alert('Added Successfully');
      location.replace('addplace.php');
      </script>";
      
     }
     else
     {
      echo "<script>alert('Panchayat Name Already Exist');</script>";
      }
   }
 else
   {

     $sql="insert into tbl_panchayath(panchayath_name)values('$panchayath_name')";

     if(mysqli_query($conn,$sql))
       {

         echo "<script>alert('Added Successfully');location.replace('addplace.php');</script>";

       }
     else
       {
 ?>
         <script> alert("failed");</script>
<?php
       }
   }
}
?>

</div>

<div class="form">
<table  class="view_table">
    <tr>
    <th>EDIT</th>
    <th>DELETE</th>
  <th>PANCHAYATH NAME</th>
    </tr>

<?php
$records=mysqli_query($conn,"select * from tbl_panchayath where status=1");
while($data = mysqli_fetch_assoc($records))
{
?>
          <tr>
    <td><a href="Edit_Panchayath.php?panchayath_edt_id=<?php echo $data['panchayath_id']; ?>">Edit</a></td>
    <td><a href="addplace.php?panchayath_edt_id=<?php echo $data['panchayath_id']; ?>">Remove</a></td>
    <td><?php echo $data['panchayath_name']; ?></td>
      </tr>


    <?php
}

    ?>

</table>
</div>
<?php
if(isset($_GET['panchayath_edt_id']))
{
  //UPDATE table tbl_panchayath

  $panchayath_id=$_GET['panchayath_edt_id'];
  if(mysqli_query($conn,"UPDATE tbl_panchayath SET status=0 WHERE panchayath_id=$panchayath_id"))
  {
    echo "<script>alert('Panchayath Deleted');</script>";
    echo '<script type="text/javascript">
      location.replace("addplace.php");
      </script>';
  }
  else
  {
    echo "<script>alert('Updation failed');</script>";
    echo '<script type="text/javascript">
      location.replace("addplace.php");
      </script>';
  }
}

 ?>
<div style="position:relative; bottom:0; width:100%;">
<?php
//including footer file
include "../Footer.php";
?>
</div>
</body>
</html>
