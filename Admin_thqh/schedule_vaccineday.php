<?php
 session_start();
 if(!isset($_SESSION['login_user']))
 {
 	echo "<script>alert('Session Expired');</script>";
 	echo '<script type="text/javascript">
 			location.replace("../index.php");
 			</script>';
 }
//Including database connection file
include "../connection.php" ;
?>
<!DOCTYPE html>
<html>
	<head>
	<title>Schedule Vaccination Day</title>

		<!--styles file including-->
		<!-- <link rel="stylesheet" href="../ktfo_css.css"> -->
		<link rel="stylesheet" type="text/css" href="../css.css">
		<link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet"
			type="text/css" />
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
		<!--
		<script language="javascript">
			$(document).ready(function () {
				$("#txtdate").datepicker({
					minDate: 0
				});
			});
		</script>
		  -->

	</head>

	<body>

		<div>
			<?php
			include "../menu_for_folder.php" ;
			 ?>
		</div>
		<!--form to add new section-->
		<h3 align="center">SCHEDULE VACCINATION DAY</h3>

		<div class="form" >
			<form id="section_add" name="section_add" method="post" action="" onsubmit="return confirm('The Registration Link will be Enabled Once You Confirm. Are you sure to Proceed ?')">
				<table>
					<tr>
					  <td>Date<span> * </span></td>
					  <td><input id="txtdate" type="date" name="date" required="required" ></td>
					</tr>
					<tr>
					  <td>Vaccine Name<span> *</td>
							<td><select name="vaccine_id" required>
								<option value="" disabled selected>-Select-</option>
							<?php
							$records = mysqli_query($conn,"select * from tbl_vaccine");
								while($data = mysqli_fetch_array($records))
								{
										 ?>

								<option value="<?php echo $data['vaccine_id']; ?> "><?php echo $data['vaccine_name']; ?></option>
								<?php
								}
								 ?>
								</select></td></tr>


					<tr>
						  <td>Count<span> *</td>
						  <td><input type="text"  name="count" required="required" /></td>
					</tr>
					<tr>
					<?php
							//Fetching First Dose Percentage
							$result = $conn->query("SELECT firstdose_percent FROM `tbl_count_percentage` ORDER BY percentage_id DESC LIMIT 1");
							if($row = mysqli_fetch_assoc($result))
								$count_percentage = $row['firstdose_percent'];
							else
								$count_percentage =20;
					?>

						  <td>First Dose Percentage <span> * </span></td>
						  <td><input type="text" size="32" value="<?php echo $count_percentage;    ?>" name="firstdose_percent" required="required"/></td>
					</tr>
					<tr>
							<td>Starting Time<span> *</td>
							<td><input type="time"  name="starting_time" required="required" id="starting_time" /></td>
							<td>Ending Time<span> *</td>
							<td><input type="time"  name="ending_time" required="required" id="ending_time" /></td>
					</tr>
								<br><br>
					<tr>
					  <td colspan="2" align="center"><button type="cancel" >CANCEL</button>
						 <button type="submit" name="add"  onclick="return checktime()">ADD</button></td>
					</tr>
				</table>
			</form>
		</div>
		<div style="bottom:0; width:100%;">
			<?php
			//including footer file
			include "../Footer.php";
			?>
		</div>
	</body>
</html>
<?php
if(isset($_POST['add']))
{
	$date=date("Y-m-d",strtotime($_POST['date']));
	$count=$_POST['count'];
	$vaccine_id=$_POST['vaccine_id'];
	$starting_time=date("Y-m-d H:i:s",strtotime($_POST['date'].$_POST['starting_time']));
	$ending_time=date("Y-m-d H:i:s",strtotime($_POST['date'].$_POST['ending_time']));

	$firstdose_percent=$_POST['firstdose_percent'];
	//$modified_date=$_POST['modified_date'];
	$modified_date=date('Y-m-d H:i:s',strtotime($_POST['date']));


		//Fetching First Dose Percentage
		$sql_percentage="SELECT firstdose_percent FROM `tbl_count_percentage` ORDER BY percentage_id DESC LIMIT 1";
		$result = $conn->query($sql_percentage);

		if($result->num_rows)
		{
		  $row = mysqli_fetch_assoc($result);
		  $count_percentage = $row['firstdose_percent'];

			if($count_percentage!=$firstdose_percent)
			{
				//inserting the data collected from the form to tbl_count_percentage
				$query="insert into tbl_count_percentage(firstdose_percent,modified_date)value('$firstdose_percent','$modified_date')";
				mysqli_query($conn,$query);
			}
		}
		else
		{
			//inserting the data collected from the form to tbl_count_percentage
			$query="insert into tbl_count_percentage(firstdose_percent,modified_date)value('$firstdose_percent','$modified_date')";
			mysqli_query($conn,$query);
		}

		//inserting the data collected from the form to tbl_vaccination_day
		$sql1="insert into tbl_vaccination_day(date,count,vaccine_id,starting_time,ending_time,status)values('$date',$count,$vaccine_id,'$starting_time','$ending_time',1)";

		if(mysqli_query($conn,$sql1))
		{
			$sql_vdid="SELECT vd_id FROM `tbl_vaccination_day` ORDER BY vd_id DESC LIMIT 1";
			$result = $conn->query($sql_vdid);
			if($result->num_rows)
			{
				$row = mysqli_fetch_assoc($result);
				$vdid = $row['vd_id'];
				//echo $vdid."<br>";
			}
		}


		  //First and second dose count
		  $first_dose_count=ceil(($count*$firstdose_percent)/100);
		  $second_dose_count=$count-$first_dose_count;
		  //Inserting to tbl_balance the remaining vaccine doses
		  $sql2="insert into tbl_balance(vd_id,first_dose_remaining,second_dose_remaining)values($vdid,$first_dose_count,$second_dose_count)";
			if(mysqli_query($conn,$sql2))
				{
				  echo "<script>alert('Added Successfully');</script>";
				  echo '<script type="text/javascript">
					location.replace("../home.php");
					</script>';
				 }
			else
				{
	?>
					<script> alert("failed");</script>
	<?php
				}
		}

?>
<script type="text/javascript"language="javascript">

           var today = new Date(); //Today Date
           var dd = today.getDate();
           var mm = today.getMonth()+1; //January is 0 so need to add 1 to make it 1!
          var yyyy = today.getFullYear();
          if(dd<10){
            dd='0'+dd
          }
          if(mm<10){
            mm='0'+mm
          }

          today = yyyy+'-'+mm+'-'+dd;
       document.getElementById("txtdate").setAttribute("min", today);

</script>

<script type="text/javascript">
function checktime()
{


      var stime=*document.getElementById("starting_time").value*;
      var etime=*document.getElementById("ending_time").value*;
      if(stime>etime){
         alert("Hai");
 }

}
</script>
