<?php
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
//Including database connection file
include "../connection.php" ;
?>
<!DOCTYPE html>
<html>
<head>
	<title>Add New Vaccine</title>

	<!--styles file including-->
	<link rel="stylesheet" type="text/css" href="../css.css">
	<style>
	a{
		text-decoration: none;
	}
	</style>
</head>

<body>
	<!--Header-->
	<div>
	<?php
	 include "../menu_for_folder.php" ;
	 ?>
	</div>

	<!--form to add new vaccine-->
	<h3 align="center">ADD NEW VACCINE</h3>
	<div class="form" >
		<form id="vaccine_add" name="vaccine_add" method="post" action="" onSubmit="return submit_form()">
		  <table>
			  <center>
				<tr>
				  <td>Vaccine Name<span> * </span></td>
				  <td><input type="text" name="vaccine_name" required="required"/></td>
				</tr>

				<tr>
				  <td colspan="2" align="center"><button type="reset" >RESET</button>
					 <button type="submit" name="add">ADD</button></td>
				</tr>
			  </center>
			</table>
		</form>
	</div>

	<div>

        <div class="form">
			<table  class="view_table">
    				<tr>
  					<th>EDIT</th>
					<th>VACCINE NAME</th>
    				</tr>

			<?php
				$records=mysqli_query($conn,"select * from tbl_vaccine");
				while($data = mysqli_fetch_assoc($records))
				{
			?>
              		<tr>
						<td><a href="Edit_vaccine.php?vaccine_edt_id=<?php echo $data['vaccine_id']; ?>">Edit</a></td>
                  		<td><?php echo $data['vaccine_name']; ?></td>
       				</tr>


            <?php
				}
            ?>

			</table>
		</div>
<br>
	<div style="position:relative; bottom:0; width:100%;">
		<?php
		//including footer file
		include "../Footer.php";
		?>
	</div>
</body>
</html>
<?php
 if(isset($_POST['add']))
 {

 	$vaccine_name=$_POST['vaccine_name'];

	$result=mysqli_query($conn,"select * from tbl_vaccine where vaccine_name='$vaccine_name'");
	if($result->num_rows>0)
	 	{

			echo "<script>alert('Vaccine name is already available.');</script>";

		}
	else
		{

			$sql="insert into tbl_vaccine(vaccine_name)values('$vaccine_name')";

			if(mysqli_query($conn,$sql))
				{

					echo "<script>alert('Added Successfully');
					location.reload();</script>";

				}
			else
				{
	?>
					<script> alert("failed");</script>
<?php
				}
		}
 }
?>
