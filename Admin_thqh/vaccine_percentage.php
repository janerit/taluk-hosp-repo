<?php
 session_start();
 if(!isset($_SESSION['login_user']))
 {
 	echo "<script>alert('Session Expired');</script>";
 	echo '<script type="text/javascript">
 			location.replace("../index.php");
			</script>';
 }
//Including database connection file
include "../connection.php" ;
?>
<!DOCTYPE html>
<html>
<head>
<title>Add Vaccine Percentage</title>

<!--styles file including-->
<!-- <link rel="stylesheet" href="../ktfo_css.css"> -->
<link rel="stylesheet" type="text/css" href="../css.css">
<link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet"
    type="text/css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script language="javascript">
    $(document).ready(function () {
        $("#txtdate").datepicker({
            minDate: 0
        });
    });
</script>
</head>
<body>

<div>
	<?php
	 include "../menu_for_folder.php" ;
	 ?>
</div>
<!--form to add new section-->
<h3 align="center">ADD VACCINE PERCENTAGE</h3>
<div class="form" >
<form id="section_add" name="section_add" method="post" action="" onSubmit="return submit_form()">
  <table>
    <tr>
      <td>First Dose Percentage <span> * </span></td>
      <td><input type="text" size="32" value="20" name="firstdose_percent" required="required"/></td>
    </tr>
	<tr>
	  <td>Date<span> * </td>
      <td><input id="txtdate" type="text" name="modified_date" required="required"></td>
    </tr>

    <tr>
      <td colspan="2" align="center"><button type="cancel">CANCEL</button>
	  	 <button type="submit" name="add">ADD</button></td>
    </tr>
  </table>
</form>
</div>
<div style="position:relative; bottom:0; width:100%;">
<?php
//including footer file
include "../Footer.php";
?>
</body>
</html>
<?php
 if(isset($_POST['add']))
 {
 	$firstdose_percent=$_POST['firstdose_percent'];
	//$modified_date=$_POST['modified_date'];
$modified_date=date('Y-m-d H:i:s',strtotime($_POST['modified_date']));




	$sql="insert into tbl_count_percentage(firstdose_percent,modified_date)value('$firstdose_percent','$modified_date')";

  	if(mysqli_query($conn,$sql))
	 	{
      echo "<script>alert('Added Successfully!');</script>";
     	echo '<script type="text/javascript">
     			location.replace("../home.php");
    			</script>';
		}
	else
		{
	?>
  	<script> alert("failed");</script>
<?php
		}
}
?>
