<?php
      	session_start();
      	if(!isset($_SESSION['login_user']))
      	{
      		echo "<script>alert('Session Expired');</script>";
      		echo '<script type="text/javascript">
      				location.replace("index.php");
      				</script>';
      	}


      	//including database connection file
      	include "connection.php" ;
      	########################  PAGE FOR EDITING SCHEDULED DAY FOR VACCINE  ########################

?>
<!DOCTYPE html>
<html>
<head>
	<title>Active Schedules</title>

	<!--styles file including-->
	<!-- <link rel="stylesheet" href="ktfo_css.css"> -->
  	<link rel="stylesheet" type="text/css" href="css.css">
	<style>
	a{
		text-decoration: none;
	}
	</style>
  <script src="https://code.jquery.com/jquery-latest.js"></script>

  <script>
  $(document).ready(function(){
       $("#table").load("slot_load.php");
       setInterval(function() {
            $("#table").load("slot_load.php");
          },1000);
    });
  </script>
  <link rel="icon" href="favicon.ico" type="image/ico">
</head>

<body>
	<!--Header-->
	<div>
	<?php
	 include "menu.php" ;
	 ?>
	</div>
<?php

if(isset($_GET['vaccine_day_edt_id']))
{
  //UPDATE table tbl_vaccine
  $vd_id=$_GET['vaccine_day_edt_id'];
  $date=date('d/m/Y',strtotime($_GET['vaccine_day']));
  if(mysqli_query($conn,"UPDATE tbl_vaccination_day SET status=0 WHERE vd_id=$vd_id"))
  {
    echo "<script>alert('Vaccination Scheduled on $date is Cancelled');</script>";
    echo '<script type="text/javascript">
      location.replace("home.php");
      </script>';
  }
  else
  {
    echo "<script>alert('Updation failed');</script>";
    echo '<script type="text/javascript">
      location.replace("home.php");
      </script>';
  }
}

 ?>
<div class="form" id="table">

</div><br><br><br><br>
<div style="bottom:0; width:100%;position:relative">
<?php
//including footer file
include "Footer.php";
?>
</div>
</body>

</html>
