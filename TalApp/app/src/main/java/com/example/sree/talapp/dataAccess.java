package com.example.sree.talapp;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Pattern;

public class dataAccess extends AsyncTask<Void,Void,String[]> {

    @Override
    protected String[] doInBackground(Void... voids) {

        ArrayList<String> msg = new ArrayList<String>();
        ArrayList<String> mobile = new ArrayList<String>();
        //String filename=new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date());
        String filename="2021-02-08-10-23-33";
       //Pattern pattern = Pattern.compile("[^(.+?),]");
        try {
            URL url = new URL("https://192.168.43.80/"+filename+".csv");
            HttpTrustManager.allowAllSSL();
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream inputStream1 = connection.getInputStream();
            BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream1));
            Scanner sc = new Scanner(bf);
            sc.useDelimiter(",");
            Log.e("filename",new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date()));
            File path=new File(Environment.getExternalStorageDirectory().getPath());
            File List=new File(path,"List.txt");
            FileWriter writer=new FileWriter(List);
            writer.append(filename+".csv");
            writer.close();
            while (sc.hasNextLine()) {

                String line1 = sc.nextLine();
                String[] arr = line1.split(",");
                msg.add(arr[0]);
                mobile.add(arr[1]);

            }
            sc.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ERROR", "Exception: " + e);
        }
        sendSMS(msg,mobile);
        //delete file content
        return null;
    }

    private void sendSMS(ArrayList<String> msg, ArrayList<String> mobile) {
       // PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        SmsManager sm = SmsManager.getDefault();
        int count = mobile.size();
        int i = 1;
        while (count > 1) {
            sm.sendTextMessage(mobile.get(i), null, msg.get(i), null, null);
         //   Log.i("IandCount", i + " and " + count);
         //   Toast.makeText(getApplicationContext(), "Send to " + mobile.get(i), Toast.LENGTH_LONG).show();
            count--;
            i++;
          //  tv.setText(String.valueOf(i));
        }

    }
}
