<?php
	include 'connection.php';

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>


		<?php
		date_default_timezone_set("Asia/Kolkata");
		$date=date('Y-m-d',time());
		$records=mysqli_query($conn,"select * from tbl_vaccination_day v INNER JOIN tbl_balance t on t.vd_id=v.vd_id where v.date >= '$date' OR v.status=1 order by v.status desc ,v.vd_id desc");
		if($records->num_rows>0)
		{

		?>
							<table  class='view_table'>
							  <tr>
							  			<td colspan='8' style='text-align: center;vertical-align: middle;'><b>ACTIVE SCHEDULED DATES</b> </td>
							  </tr>
							  <tr>
										  <th>Date</th>
										  <th>Vaccine</th>
										  <th>Total Count</th>
										  <th>Starting Time</th>
										  <th>Ending Time</th>
										  <th>Slot Remaining (1st Dose)</th>
										  <th>Slot Remaining (2nd Dose)</th>
										  <th>Close</th>
							    </tr>
							<?php
							while($data = mysqli_fetch_assoc($records))
							{
							?>
							    <tr>

							    <td><?php echo  date('d/m/Y',strtotime($data['date'])); ?></td>
							    <?php
							    $vaccine_id=$data['vaccine_id'];
							    $record=mysqli_query($conn,"select vaccine_name from tbl_vaccine where vaccine_id=$vaccine_id");
							    $datas = mysqli_fetch_assoc($record);
							    $start_time=$data['starting_time'];
							    $start_time = strtotime($start_time);
							    $ending_time=$data['ending_time'];
							    $ending_time = strtotime($ending_time);
									$vd_id=$data['vd_id']
							    ?>

							    <td><?php echo $datas['vaccine_name']; ?></td>
							    <td><?php echo $data['count']; ?></td>
							    <td><?php echo date('H:i:s', $start_time); ?></td>
							    <td><?php echo date('H:i:s', $ending_time); ?></td>
							    <td><?php echo $data['first_dose_remaining']; ?></td>
							    <td><?php echo $data['second_dose_remaining']; ?></td>
									<?php
													$closed=mysqli_query($conn,"select * from tbl_vaccination_day where status=1 and vd_id=$vd_id");
													if($closed->num_rows>0)
													{
									 ?>
											<td><a href="home.php?vaccine_day_edt_id=<?php echo $data['vd_id']; ?> & vaccine_day=<?php echo $data['date']; ?>" onclick="return confirm('Are you sure about the cancellation?')">Click Here </a></td>
										<?php
													}
													else {
													?>
													<td>Closed</td>
										<?php
													}
										 ?>
									</tr>


							    <?php
							}
							    ?>

		</table>
		<?php
		}
		else {
		?>
		<h4>No Active Schedules</h4>
		<?php
}
		?>
	</body>
</html>
