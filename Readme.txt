Take terminal and give the following
------------------------------------


// Clone the repository taluk-hosp-repo using the link from bitbucket. Then change to the direcory taluk-hosp-repo
cd taluk-hosp-repo

//Build the image named cswl/xampp (windows users, no sudo; non debians, please su to root)
sudo docker build -t cswl/xampp .

//Create the container with the name vaccination_container
sudo docker create --restart unless-stopped -it -p 8089:80 -v $(pwd)/:/www --name vaccination_container cswl/xampp

//For Windows users 
sudo docker create --restart unless-stopped -it -p 8089:80 -v %cd%/:/www --name vaccination_container cswl/xampp
//A container with a volume is created. 

//start the container
sudo docker container start -i -a vaccination_container

//Dump the sql file 
mysql -u root < Talukhos_vaccination.sql

// To detach from container press Ctrl+p+q

Then open the browser and give 127.0.0.1:8089/www

For subsequent executions, put your code inside the volume (i.e. taluk-hosp-repo/ folder)  and refresh the browser.
If something not working, you can stop the container and start it again by any one of the following:

  1) sudo docker container stop vaccination_container
     sudo docker container start vaccination_container
		OR
  2) sudo docker container restart vaccination_container
 


Database details
****************

root password - nil (blank)

username - developer
password - passwd#1234
database - Talukhos_vaccination

You can access the database through 
	mysql -uroot Talukhos_vaccination
		or
	mysql -udeveloper -ppasswd#1234 Talukhos_vaccination
inside the container


please share the mistakes/vagueness you find in the whatsapp group..

Happy Coding...!!
