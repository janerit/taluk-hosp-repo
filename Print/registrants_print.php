<?php
session_start();
include '../connection.php';
require_once('../FPDF/fpdf.php');
require_once('../FPDI/src/autoload.php');
use \setasign\Fpdi\Fpdi;

$date=$_SESSION['date'];
$vaccine_id=$_SESSION['vaccine_id'];
$output="OnDate-".$date.".pdf";
$vaccine_name=$_SESSION['vaccine_name'];
$public=mysqli_query($conn,"SELECT * FROM tbl_slot s
   INNER JOIN tbl_public p on p.public_id=s.public_id
   INNER JOIN tbl_vaccination_day vd on s.vd_id=vd.vd_id
   INNER JOIN tbl_vaccine v on v.vaccine_id=vd.vaccine_id where vd.date='$date' and vd.vaccine_id=$vaccine_id order by token_no");
date_default_timezone_set("Asia/Kolkata");

generatePDF("Registrants.pdf",$output,"blank.pdf",$public,$date,$vaccine_name);

function generatePDF($source, $output,$blank,$public,$date,$vaccine_name)
{
      $pdf = new FPDI();

      $pdf->AddPage();
      $count=1;
      $new_counter=0; // counter to check rows in the else part
        $x=76; #y-cord
        $pagecount = $pdf->setSourceFile($source);
        $tppl = $pdf->importPage(1);
        $pdf->useTemplate($tppl, 0, 0); //need to be set , 210, 148


        $pdf->SetFont('Times','',11); // Font Name, Font Style (eg. 'B' for Bold), Font Size
        $pdf->SetTextColor(0,0,0); // RGB
        $pdf->SetXY(152.8,37.8); // X start, Y start in mm
        $pdf->Write(0,$date);

        $pdf->SetFont('Times','',11); // Font Name, Font Style (eg. 'B' for Bold), Font Size
        $pdf->SetTextColor(0,0,0); // RGB
        $pdf->SetXY(152.8,45.8); // X start, Y start in mm
        $pdf->Write(0,$vaccine_name);


        while($data = mysqli_fetch_assoc($public))
        {



          if($count<=24)
                {


                           //Add A4 by Default






                          $pdf->SetFont('Arial','',10);
                          $pdf->SetXY(25.5,$x);
                          $pdf->Cell(16.9,9,$data['token_no'],1,0,'C');
                          $pdf->Cell(63.0011,9,$data['name'],1,0,'L');
                          $pdf->Cell(12.5,9,$data['age'],1,0,'L');
                          $pdf->Cell(27.5,9,$data['aadhar'],1,0,'L');
                          $pdf->Cell(30.05,9,$data['mobile_no'],1,0,'L');
                          $pdf->Cell(11.8,9,$data['dose_no'],1,0,'L');
                          $pdf->Cell(10.5,9,'',1,0,'L');
                          $count++;
                          $x=$x+8;
                      }
                      else if ($count==25)
                      {
                            $pdf->AddPage();

                            $pagecount = $pdf->setSourceFile($blank);
                            $tppl = $pdf->importPage(1);
                            $pdf->useTemplate($tppl, 0, 0); //need to be set , 210, 148

                            $y=27;

                             $y=$y+8;
                             $pdf->SetFont('Arial','',10);
                            $pdf->SetXY(25.35,$y);
                            $pdf->Cell(16.9,9,$data['token_no'],1,0,'C');
                            $pdf->Cell(63.0011,9,$data['name'],1,0,'L');
                            $pdf->Cell(12.5,9,$data['age'],1,0,'L');
                            $pdf->Cell(27.5,9,$data['aadhar'],1,0,'L');
                            $pdf->Cell(30.05,9,$data['mobile_no'],1,0,'L');
                            $pdf->Cell(11.8,9,$data['dose_no'],1,0,'L');
                            $pdf->Cell(10.5,9,'',1,0,'L');
                            $count++;

                        }
                        else
                        {

                          $new_counter++;
                          $y=$y+8;
                          $pdf->SetXY(25.35,$y);
                          $pdf->Cell(16.9,9,$data['token_no'],1,0,'C');
                          $pdf->Cell(63.0011,9,$data['name'],1,0,'L');
                          $pdf->Cell(12.5,9,$data['age'],1,0,'L');
                          $pdf->Cell(27.5,9,$data['aadhar'],1,0,'L');
                          $pdf->Cell(30.05,9,$data['mobile_no'],1,0,'L');
                          $pdf->Cell(11.8,9,$data['dose_no'],1,0,'L');
                          $pdf->Cell(10.5,9,'',1,0,'L');
                          $count++;
                          if($new_counter==27)
                          {
                            $count=25;
                            $new_counter=0;
                          }

                        }

            }

  $pdf->Output($output,"I");
}

?>
